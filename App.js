import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, ScrollView} from 'react-native';

export default class App extends Component {

  render() {
    return (
      <View style={styles.main}>
        <ScrollView>
        <View style={styles.top}>
          <View style={styles.ttext}>
            <Text style={styles.ttext1}>-</Text>
          </View>
          <View style={styles.ttext2}></View>
          <Text style={styles.ttext3}>Healthy Fruits</Text>
        </View>
        <View style={styles.header}>
          <View style={styles.hleft}>
            <Text style={styles.hltext1}>Lorem Ipsum</Text>
            <Text style={styles.hltext2}>Lorem ipsum is simply dummy text of the{'\n'}printing and typesetting industry. Lorem</Text>
          </View>
          <View style={styles.hright}>
            <Image style={styles.hrimage} source = {require('./src/images/fruits.png')} />
          </View>
        </View>
        <View style={styles.hborder}></View>
        <View style={styles.middle}>
          <View style={styles.mleft}>
            <Image style={styles.mlimage} source = {require('./src/images/avocado.png')} />
          </View>
          <View style={styles.mright}>
            <Text style={styles.mrtext1}>Lorem Ipsum</Text>
            <Text style={styles.mrtext2}>Lorem ipsum is simply dummy text of the{'\n'}printing and typesetting industry. Lorem{'\n'}Ipsum has been the industry's standard</Text>
          </View>
        </View>
        <View style={styles.lower}>
          <View style={styles.lleft}>
            <Text style={styles.lltext1}>Lorem Ipsum</Text>
            <Text style={styles.lltext2}>Lorem ipsum is simply dummy text of the{'\n'}printing and typesetting industry. Lorem</Text>
          </View>
          <View style={styles.lright}>
            <Image style={styles.lrimage} source = {require('./src/images/fruits.png')} />
          </View>
        </View>
        <View style={styles.lborder}></View>
        <View style={styles.last}>
          <View style={styles.lastmain}>
            <Text style={styles.lastmaintext}>Nutrition fact</Text>
            <View style={styles.lastsub}>
              <Text style={styles.lastsubtext1}>Serving Size</Text>
              <Text style={styles.lastsubtext2}>100g</Text>
            </View>
            <View style={styles.lastbold}/>
            <Text style={styles.lastsubtext1}>Amount per serving</Text>
            <View style={styles.lastsub}>
              <Text style={styles.lastmaintext2}>Calories</Text>
              <Text style={styles.lastmaintext}>89</Text>
            </View>
            <View style={styles.lastbold}/>
            <View style={styles.lastsub}>
              <Text style={styles.lastsubtext1}></Text>
              <Text style={styles.lastsubtext2}>% Daily value *</Text>
            </View>
            <View style={styles.lastnormal}/>
            <View style={styles.lastsub}>
              <Text style={styles.lastsubtext1}><Text style={{fontWeight:'bold'}}>Toatal fat</Text> 0.3g</Text>
              <Text style={styles.lastsubtext2}>0%</Text>
            </View>
            <View style={styles.lastnormal}/>
            <View style={styles.lastsub}>
              <Text style={styles.lastsubtext1}>  Saturated fat 0.1g</Text>
              <Text style={styles.lastsubtext2}>0%</Text>
            </View>
            <View style={styles.lastnormal}/>
            <View style={styles.lastsub}>
              <Text style={styles.lastsubtext1}><Text style={{fontWeight:'bold'}}>Sodium</Text> 1mg</Text>
              <Text style={styles.lastsubtext2}>0%</Text>
            </View>
            <View style={styles.lastnormal}/>
            <View style={styles.lastsub}>
              <Text style={styles.lastsubtext1}><Text style={{fontWeight:'bold'}}>Total carbohydrate</Text> 23g</Text>
              <Text style={styles.lastsubtext2}>8%</Text>
            </View>
            <View style={styles.lastnormal}/>
            <View style={styles.lastsub}>
              <Text style={styles.lastsubtext1}>  Dietary fiber 2.6g</Text>
              <Text style={styles.lastsubtext2}>9%</Text>
            </View>
            <View style={styles.lastnormal}/>
            <View style={styles.lastsub}>
              <Text style={styles.lastsubtext1}>  Sugar 12g</Text>
              <Text style={styles.lastsubtext2}></Text>
            </View>
            <View style={styles.lastnormal}/>
            <View style={styles.lastsub}>
              <Text style={styles.lastsubtext1}><Text style={{fontWeight:'bold'}}>Protein</Text> 1.1g</Text>
              <Text style={styles.lastsubtext2}>2%</Text>
            </View>
            <View style={styles.lastbold}/>
            <View style={styles.lastsub}>
              <Text style={styles.lastsubtext1}>Vitamin D 0.00mcg</Text>
              <Text style={styles.lastsubtext2}>0%</Text>
            </View>
            <View style={styles.lastnormal}/>
            <View style={styles.lastsub}>
              <Text style={styles.lastsubtext1}>Calcium 5.00mg</Text>
              <Text style={styles.lastsubtext2}>0%</Text>
            </View>
            <View style={styles.lastnormal}/>
            <View style={styles.lastsub}>
              <Text style={styles.lastsubtext1}>Iron 0.26mg</Text>
              <Text style={styles.lastsubtext2}>1%</Text>
            </View>
            <View style={styles.lastnormal}/>
            <View style={styles.lastsub}>
              <Text style={styles.lastsubtext1}>Potassium 358mg</Text>
              <Text style={styles.lastsubtext2}>8%</Text>
            </View>
            <View style={styles.lastbold}/>
              <Text style={styles.lastsubtext1}>The daily value (DV) tells you how much a nutrient in a serving of food contribute to a daily diet 2000 calaries a day is used for general nutrition advice.</Text>
          </View>
        </View>
          <Text style={styles.lastsubtextgreen}>*  Delivering with love and care for you to relish</Text>
            <View style={styles.lastbold}/>
              <Text style={styles.lastsubtext4}>About demo group</Text>
              <Text style={styles.lastsubtext4}>Leading the crops Division for Demo Group is india's best branded fruit company and a market leader in controlled cultivation and marketing of high-quality fruits to domestic and international customers  in a socially responsible manner.</Text>
              <Text style={styles.lastsubtext4}>We are happy to serve fresh & delicious fruits</Text>
            <View style={styles.lastbold}/>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  main:{
    backgroundColor:'white',
    height:'100%',
    width:'100%',
  },
  top:{
    flexDirection:'row',
    marginVertical:2,
  },
  ttext:{
    backgroundColor:'black',
    marginHorizontal:5,
    borderRadius:50
  },
  ttext1:{
    color:'white',
    paddingHorizontal:7.5,
    fontSize:15
  },
  ttext2:{
    borderLeftWidth:1,
    borderLeftColor:'black'
  },
  ttext3:{
    marginHorizontal:5,
  },
  header:{
    backgroundColor:'#edb51c',
    flexDirection:'row',
    justifyContent:'space-between',
    width:'100%',
    // height:'23%',
  },
  hleft:{
    width:'50%',
    alignSelf:'center',
  },
  hltext1:{
    fontSize:30,
    textAlign:'center',
    color:'brown',
    fontStyle:'italic',
  },
  hltext2:{
    fontSize:10,
    textAlign:'center',
    color:'white',
    marginTop:10
  },
  hright:{
    alignSelf:'flex-end',
  },
  hrimage:{
    width:120,
    height:120,
    marginTop:'5%',
  },
  hborder:{
    backgroundColor:'brown',
    height:'0.5%'
  },
  middle:{
    backgroundColor:'white',
    flexDirection:'row',
    justifyContent:'space-between',
    width:'100%',
    // height:'23%',
  },
  mleft:{
    alignItems:'center',
    width:'50%'
  },
  mlimage:{
    width:120,
    height:120,
  },
  mright:{
    alignSelf:'center',
    width:'50%'
  },
  mrtext1:{
    fontSize:25,
    textAlign:'center',
    color:'green',
    fontStyle:'italic',
  },
  mrtext2:{
    fontSize:9,
    textAlign:'center',
    color:'green',
    marginTop:10
  },
  lower:{
    backgroundColor:'green',
    flexDirection:'row',
    justifyContent:'space-between',
    width:'100%',
    // height:'23%',
  },
  lleft:{
    width:'50%',
    alignSelf:'center',
  },
  lltext1:{
    fontSize:30,
    textAlign:'center',
    color:'yellow',
    fontStyle:'italic',
  },
  lltext2:{
    fontSize:10,
    textAlign:'center',
    color:'white',
    marginTop:10
  },
  lright:{
    alignSelf:'flex-end',
  },
  lrimage:{
    width:120,
    height:120,
    marginTop:'5%',
  },
  lborder:{
    backgroundColor:'lightblue',
    height:'1%'
  },
  last:{
    borderWidth:1,
    borderColor:'grey',
    borderRadius:10,
    marginHorizontal:5,
    marginBottom:'5%',
  },
  lastmain:{
    padding:5,
  },
  lastmaintext:{
    fontSize:18,
    fontWeight:'bold'
  },
  lastmaintext2:{
    fontSize:20,
    fontWeight:'bold'
  },
  lastsub:{
    flexDirection:'row',
    justifyContent:'space-between'
  },
  lastbold:{
    height:3,
    backgroundColor:'black',
    marginVertical:3
  },
  lastnormal:{
    height:1,
    backgroundColor:'black',
    marginVertical:2
  },
  lastsubtextgreen:{
    color:'green',
    fontSize:18,
    marginBottom:'3%',
  },
  lastsubtext4:{
    marginBottom:'3%',
  }
})